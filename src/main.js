import Vue from 'vue'
import App from './App.vue'
// import Students from './components/Students.vue' //this is to register and use this component globally.if you want to see how to do this locally see App.vue which is done with Helloworld.vue

Vue.config.productionTip = false

// Vue.component('stud', Students)//this is to register and use this component globally.

new Vue({
  render: h => h(App),
}).$mount('#app')
